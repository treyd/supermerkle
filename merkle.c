/*
 * Copyright 2020 Trey Del Bonis
 *
 * This is distributed under the terms of GPLv3, the one true software license.
 */

#include <stddef.h>

#include "merkle.h"

#define DBprint(f, ...)                                                        \
  {}
#define DBlog(m)                                                               \
  {}

static size_t count_chunks(size_t len) {
  size_t chunks = len / MERKLE_CHUNK_SIZE;
  if (len % MERKLE_CHUNK_SIZE > 0) {
    chunks++;
  }
  return chunks;
}

static size_t calc_proof_len(size_t n_chunks) {
  int depth = 0;
  while (n_chunks >>= 1) {
    depth++;
  }
  return depth;
}

size_t merkle_ct_hash_eq(merklehash *a, merklehash *b) {
  // We do this with a weird accumulator to avoid making a prefix oracle.
  size_t acc = 0;
  for (size_t i = 0; i < SHA256_BLOCK_SIZE; i++) {
    acc += ((size_t)a->buf[i]) ^ ((size_t)b->buf[i]);
  }
  return acc == 0 ? 0 : 1;
}

// Update merkle_hash_chunk and merkle_hash_node if you change the lengths of
// these!
static char *MERKLE_NODE_TAG = "node";
static char *MERKLE_LEAF_TAG = "leaf";

void merkle_hash_chunk(BYTE *buf, size_t buf_len, size_t chunk_index,
                       merklehash *dest) {
  SHA256_CTX ctx;
  sha256_init(&ctx);

  // Do some math to figure out how much we should actually read into the
  // digest.
  size_t start_off = chunk_index * MERKLE_CHUNK_SIZE;

  // If we're way past the end then as an optimization we do this right off the
  // bat.
  if (start_off >= buf_len || buf == NULL) {
    BYTE zeros[64] = {0};
    for (size_t i = 0; i < SHA256_BLOCK_SIZE / 64; i++) {
      sha256_update(&ctx, (BYTE *)&zeros, 64);
    }
    sha256_update(&ctx, (unsigned char *)MERKLE_LEAF_TAG, 4);
    sha256_final(&ctx, dest->buf);
    return;
  }

  size_t buf_to_read = buf_len - start_off;
  if (buf_to_read > MERKLE_CHUNK_SIZE) {
    buf_to_read = MERKLE_CHUNK_SIZE;
  }

  size_t bytes_remaining = MERKLE_CHUNK_SIZE;
  if (buf_to_read > 0) {
    sha256_update(&ctx, buf + start_off, buf_to_read);
    bytes_remaining -= buf_to_read;
  }

  // Pad out the rest with zeros.
  while (bytes_remaining > 0) {
    BYTE zero[] = {0};
    sha256_update(&ctx, (BYTE *)&zero, 1);
    bytes_remaining--;
  }

  // Then just fill in the destination buffer with the data provided.
  sha256_update(&ctx, (unsigned char *)MERKLE_LEAF_TAG, 4);
  sha256_final(&ctx, dest->buf);
}

void merkle_hash_node(merklehash *left, merklehash *right, merklehash *dest) {
  SHA256_CTX ctx;
  sha256_init(&ctx);

  // We should be able to do this in 2 separate ops and it's just as good, no
  // need to do a copy here.
  sha256_update(&ctx, (BYTE *)left, SHA256_BLOCK_SIZE);
  sha256_update(&ctx, (BYTE *)right, SHA256_BLOCK_SIZE);

  // Now do a thing to make it hard to brute force things.
  sha256_update(&ctx, (unsigned char *)MERKLE_NODE_TAG, 4);
  sha256_final(&ctx, (BYTE *)dest);
}

static void hash_of_empty(merklehash *dest) {
  SHA256_CTX ctx;
  sha256_init(&ctx);
  sha256_final(&ctx, (BYTE *)dest->buf);
}

static void merkle_hash_subtree(BYTE *buf, size_t buf_len, size_t depth,
                                size_t subtree_index, merklehash *dest) {
  if (depth > 0) {
    merklehash left;
    merklehash right;
    merkle_hash_subtree(buf, buf_len, depth - 1, subtree_index * 2, &left);
    merkle_hash_subtree(buf, buf_len, depth - 1, subtree_index * 2 + 1, &right);
    merkle_hash_node(&left, &right, dest);
  } else {
    // We don't have to validate the subtree_index here since we'll just get
    // a hash of null bytes.  Which is what we want.
    merkle_hash_chunk(buf, buf_len, subtree_index, dest);
  }
}

void merkle_compute_root(BYTE *buf, size_t buf_len, merklehash *dest) {
  if (buf_len == 0) {
    hash_of_empty(dest);
    return;
  }

  size_t proof_len = calc_proof_len(count_chunks(buf_len));

  // Then just call the function recursively.
  merkle_hash_subtree(buf, buf_len, proof_len + 1, 0, dest);
}

void merklemr_init_empty(merklemr *mmr) {
  for (size_t i = 0; i < MAX_PROOF_LEN * SHA256_BLOCK_SIZE; i++) {
    mmr->peaks[i / SHA256_BLOCK_SIZE].buf[i % SHA256_BLOCK_SIZE] = (BYTE)0;
  }
  mmr->num = 0;
}

void merklemr_add_hash(merklemr *mmr, merklehash *next) {
  // Don't overflow the accumulator, because if we do things break.
  if (mmr->num >= (1 << MAX_PROOF_LEN)) {
    return;
  }

  // If it's 0 then we just has it by itself.
  if (mmr->num == 0) {
    mmr->peaks[0] = *next;
    mmr->num++;
    return;
  }

  // Now we go through the tree and merge together subtrees.
  size_t peak_mask = mmr->num;
  merklehash cur_node = *next;
  size_t cur_height = 0;
  while ((peak_mask >> cur_height) & 1) {
    merklehash next_node;
    merkle_hash_node(&mmr->peaks[cur_height], &cur_node, &next_node);

    // Set it to FFs for debugging purposes.
    for (size_t i = 0; i < SHA256_BLOCK_SIZE; i++) {
      mmr->peaks[cur_height].buf[i] = 0xff;
    }

    cur_node = next_node;
    cur_height++;
  }

  // Finally store the cur node at the height we got to.
  mmr->peaks[cur_height] = cur_node;
  mmr->num++;
}

void merkle_proof_init_empty(merkleproof *proof) {
  // Fill in an "empty" proof for the single element.
  proof->prooflen = 0;
  for (size_t i = 0; i < MAX_PROOF_LEN; i++) {
    for (size_t j = 0; j < SHA256_BLOCK_SIZE; j++) {
      proof->cohashes[i].buf[j] = 0;
    }
  }
}

/// Computes the subtree index of a particular leaf for subtrees of some height,
/// helpful in updating proofs.
#define subtree_index_at_height(h0idx, height) ((h0idx) >> (height))

void merklemr_add_hash_update(merklemr *mmr, merklehash *next,
                              merkleproof *pdest, merkleproof *updatev,
                              size_t *idxv, size_t update_num) {
  // Don't overflow the accumulator, because if we do things break.
  if (mmr->num >= (1 << MAX_PROOF_LEN)) {
    DBlog("overflowed MMR wtf");
    return;
  }

  // If it's 0 then we just has it by itself.
  if (mmr->num == 0) {
    if (pdest != NULL) {
      merkle_proof_init_empty(pdest);
    }

    // Init the MMR with the single element.
    mmr->peaks[0] = *next;
    mmr->num++;

    return;
  }

  // Now we go through the tree and merge together subtrees.
  size_t new_chunk_index = mmr->num;
  size_t peak_mask = mmr->num;
  merklehash cur_node = *next;
  size_t cur_height = 0;
  while ((peak_mask >> cur_height) & 1) {
    // This is the root of the subtree that was already there.
    merklehash *prev_node = &mmr->peaks[cur_height];

    // Compute the new subtree node.
    merklehash next_node;
    merkle_hash_node(prev_node, &cur_node, &next_node);

    // Update each step of the new proof we're generating.
    if (pdest != NULL) {
      // Set the subtree hash in the new proof to the previous node.
      pdest->cohashes[cur_height] = *prev_node;
      pdest->prooflen++;
    }

    // "chunk parent tree"
    size_t c_pt = subtree_index_at_height(new_chunk_index, cur_height + 1);

    // Now go through and add all new new cohashes, if it applies.
    for (size_t i = 0; i < update_num; i++) {
      merkleproof *p = &updatev[i];
      size_t pc = idxv[i];

      // "proof('s chunk) parent tree"
      size_t p_pt = subtree_index_at_height(pc, cur_height + 1);

      if (c_pt == p_pt) {
        if ((pc >> cur_height) & 1) {
          // If we're on the right side, add the left node.
          p->cohashes[cur_height] = *prev_node;
          p->prooflen++;
        } else {
          // If we're on the left side, add the right node.
          p->cohashes[cur_height] = cur_node;
          p->prooflen++;
        }
      }
    }

    // Set it to FFs for debugging purposes.
    for (size_t i = 0; i < SHA256_BLOCK_SIZE; i++) {
      mmr->peaks[cur_height].buf[i] = 0xff;
    }

    cur_node = next_node;
    cur_height++;
  }

  if (pdest != NULL) {
    // Set the more general parameters of the proof.
    pdest->prooflen = cur_height;
  }

  if (cur_height >= MAX_PROOF_LEN) {
    DBlog("MAX PROOF LEN EXCEEDED");
  }

  // Finally store the cur node at the height we got to.
  mmr->peaks[cur_height] = cur_node;
  mmr->num++;
}

size_t merklemr_get_single_root(merklemr *mmr, merklehash *dest) {
  if (mmr->num == 0) {
    return 2;
  }

  // Compute the index we need to go to.
  size_t i = 0;
  while ((1 << i) <= mmr->num) {
    i++;
  }
  i--; // We actually go one more than we need to.

  // Check if it's not a power-of-2.
  if (((1 << i) & mmr->num) != mmr->num) {
    return 1;
  }

  *dest = mmr->peaks[i];
  return 0;
}

void merkle_compute_root_mmr(merklemr *workspace, BYTE *buf, size_t buf_len,
                             merklehash *dest) {
  merklemr_init_empty(workspace);

  // Quick escape.
  if (buf_len == 0) {
    hash_of_empty(dest);
  }

  size_t buf_chunks = count_chunks(buf_len);

  // Compute the actual number of chunks we need to process.
  size_t chunkslog2 = 0;
  while ((1 << chunkslog2) <= buf_chunks) {
    chunkslog2++;
  }

  // Actually process all the chunks.
  size_t chunks = 1 << chunkslog2;
  merklehash tmphash;
  for (size_t i = 0; i < chunks; i++) {
    merkle_hash_chunk(buf, buf_len, i, &tmphash);
    merklemr_add_hash(workspace, &tmphash);
  }

  merklemr_get_single_root(workspace, dest);
}

size_t merkle_proof_verify(merklehash *root, size_t idx, merklehash *chunk_hash,
                           merkleproof *proof) {
  if (root == NULL || chunk_hash == NULL) {
    return 1;
  }

  // Don't read out of bounds.
  if (proof->prooflen > MAX_PROOF_LEN) {
    // we're being attacked!
    return 1337;
  }

  // Quick check in case of tree with single element.
  if (proof->prooflen == 0) {
    return merkle_ct_hash_eq(root, chunk_hash);
  }

  merklehash cur_hash = *chunk_hash;
  size_t side_flags = idx; // see comment on chunk_index in merkleproof.

  for (size_t i = 0; i < proof->prooflen; i++) {
    merklehash node_hash;
    if (side_flags & 1) {
      // This branch is on the right side, hash with the left.
      merkle_hash_node(&proof->cohashes[i], &cur_hash, &node_hash);
    } else {
      // This branch is on the left side, hash with the right.
      merkle_hash_node(&cur_hash, &proof->cohashes[i], &node_hash);
    }

    side_flags >>= 1;
    cur_hash = node_hash;
  }

  // Now check the final hash against the root we expected.
  return merkle_ct_hash_eq(&cur_hash, root);
}

size_t merklemr_proof_verify(merklemr *mmr, size_t idx, merklehash *chunk_hash,
                             merkleproof *proof) {
  // Any proof for an empty MMR is obviously not from this MMR and thus invalid.
  if (mmr->num == 0) {
    return 2;
  }

  // Quick fail check to make sure we don't do bad things.
  if (mmr->num <= idx) {
    return 3;
  }

  if (proof->prooflen > MAX_PROOF_LEN) {
    // we're being attacked (see above)
    return 1337;
  }

  // It's basically the same routine otherwise.
  return merkle_proof_verify(&mmr->peaks[proof->prooflen], idx, chunk_hash,
                             proof);
}

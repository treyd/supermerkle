/*
 * Copyright 2020 Trey Del Bonis
 *
 * This is distributed under the terms of GPLv3, the one true software license.
 */

#ifndef _SEEKER_H_
#define _SEEKER_H_

#include <stddef.h>

#include "merkle.h"

typedef struct {
  // These are the precomputed chunk hashes.
  merklehash *chunk_hashes;

  // This is how many of them there are.
  size_t data_chunks;

  // This is how many hashes we actually have to compute to make perfect tree.
  size_t total_chunks;

  // This is kinda an optimization?
  merklemr current_mmr;
} bufseeker;

/**
 * Precomputes all the checkpoints and positions the seeker at 0.
 *
 * The last argument needs to be a buffer large enough to store the hashes for
 * all of the data in the buffer, so 32*ceilpow2(buflen/chunksize) bytes.
 */
size_t seeker_init_precompute(bufseeker *, BYTE *, size_t, merklehash *);

/**
 * Computes the proof for the next chunk, and returns it and the hash.
 *
 * Returns nonzero on failure.
 */
size_t seeker_next_proof(bufseeker *, merklehash *, merkleproof *);

/**
 * Seeks to an absolute position starting from the beginning of the buffer.
 *
 * Obviously faster if you're seeking to a checkpoint since we don't have to
 * recompute anything.
 *
 * Returns 0 on success.  If there's some error, returns error code.
 */
size_t seeker_seek_absolute(bufseeker *, size_t);

#endif

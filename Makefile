all: merkle.o seeker.o sha256.o

clean:
	rm -fv *.o *~

merkle.o:
	gcc -c merkle.c

seeker.o:
	gcc -c seeker.c

sha256.o:
	gcc -c sha256.c

.PHONY: all clean

/*
 * Copyright 2020 Trey Del Bonis
 *
 * This is distributed under the terms of GPLv3, the one true software license.
 */

#include "seeker.h"

static size_t count_chunks(size_t len) {
  size_t chunks = len / MERKLE_CHUNK_SIZE;
  if (len % MERKLE_CHUNK_SIZE > 0) {
    chunks++;
  }
  return chunks;
}

size_t seeker_init_precompute(bufseeker *seeker, BYTE *buf, size_t buf_len,
                              merklehash *chunks_buf) {
  // Initialize the world.
  merklemr_init_empty(&seeker->current_mmr);
  seeker->chunk_hashes = chunks_buf;

  // Compute the actual number of chunks we need to process.
  size_t buf_chunks = count_chunks(buf_len);
  size_t chunkslog2 = 0;
  while ((1 << chunkslog2) <= buf_chunks) {
    chunkslog2++;
  }

  seeker->data_chunks = buf_chunks;
  seeker->total_chunks = 1 << chunkslog2;

  // Compute all the hashes.  Very simple.
  for (size_t i = 0; i < seeker->data_chunks; i++) {
    merkle_hash_chunk(buf, buf_len, i, &seeker->chunk_hashes[i]);
  }

  return 1;
}

size_t seeker_next_proof(bufseeker *seeker, merklehash *hashdest,
                         merkleproof *proofdest) {

  // Make sure we're not at the end of the data.
  if (seeker->current_mmr.num >= seeker->data_chunks) {
    return -1;
  }

  size_t proof_chunk = seeker->current_mmr.num;

  // Update the MMR and get the data for the next thing.
  merklehash *next_hash = &seeker->chunk_hashes[proof_chunk];
  merklemr_add_hash_update(&seeker->current_mmr, next_hash, proofdest, NULL,
                           NULL, 0);
  *hashdest = *next_hash;

  // Now fill out the rest of the proof.  Use a copy of the MMR.
  merklemr dup_mmr = seeker->current_mmr;
  merklehash empty;
  merkle_hash_chunk(NULL, 0, 0, &empty);
  for (size_t i = seeker->current_mmr.num; i < seeker->total_chunks; i++) {
    if (i < seeker->data_chunks) {
      next_hash = &seeker->chunk_hashes[i];
      merklemr_add_hash_update(&dup_mmr, next_hash, NULL, proofdest,
                               &proof_chunk, 1);
    } else {
      merklemr_add_hash_update(&dup_mmr, &empty, NULL, proofdest, &proof_chunk,
                               1);
    }
  }

  return proof_chunk;
}

size_t seeker_seek_absolute(bufseeker *seeker, size_t seek_chunk_idx) {
  // Sanity checks.
  if (seek_chunk_idx < 0 || seek_chunk_idx >= seeker->data_chunks) {
    return -1;
  }

  merklemr_init_empty(&seeker->current_mmr);

  for (size_t i = 0; i < seek_chunk_idx; i++) {
    merklemr_add_hash(&seeker->current_mmr, &seeker->chunk_hashes[i]);
  }

  return 0;
}

# supermerkle

This was created for the 2020 MITRE eCTF.  This is a cleaned up version from
what was finally submitted by our team.  That version was a modified from the
original to use blake2b and have some extra embedded hardening added to it.
This version is meant to be like the original, which I have a copy of somewhere
on a hard drive in my apartment, but I'm not sure where.

Note that sha256.{c,h} are not mine, I copied them from
[here](https://github.com/B-Con/crypto-algorithms/blob/master/sha256.h).

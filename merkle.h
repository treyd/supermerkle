/*
 * Copyright 2020 Trey Del Bonis
 *
 * This is distributed under the terms of GPLv3, the one true software license.
 */

#ifndef _MERKLE_H_
#define _MERKLE_H_

#include <stddef.h>
#include <stdint.h>

#include "sha256.h"

#ifndef MERKLE_CHUNK_SIZE
#define MERKLE_CHUNK_SIZE 1024
#endif

// This should be enough for like 256 MiB if I did my math right, assuming
// MERKLE_CHUNK_SIZE of 1024.
#define MAX_PROOF_LEN 14

typedef uint8_t BYTE;

/**
 * This is just a 32-byte array for storing hashes.
 */
typedef struct {
  BYTE buf[SHA256_BLOCK_SIZE];
} merklehash;

/**
 * Checks if two hashes are of equal sizes in constant time.  Must not be NULL.
 *
 * Return 0 if equal, returns 1 if not equal.
 */
size_t merkle_ct_hash_eq(merklehash *a, merklehash *b);

/**
 * Hashes a single chunk of data, based on index.  If the chunk index is beyond
 * the range of the buffer it just returns the hash of a null chunk.
 *
 * It's actually safe to call this on NULL as long as the length is specified as
 * zero.  It's just going to always return hashes of null chunks.
 */
void merkle_hash_chunk(BYTE *, size_t, size_t, merklehash *);

/**
 * Combiner function for finding the node hash of 2 other nodes.
 */
void merkle_hash_node(merklehash *left, merklehash *right, merklehash *dest);

/**
 * Just computes the merkle root of a buffer.  Doesn't provide proofs, also
 * probably slower than merkle_compute_root_mmr since it uses more memory.
 */
void merkle_compute_root(BYTE *, size_t, merklehash *);

/**
 * Merkle Mountain Range accumulator thingy.  I use this for making proofs in
 * really small space.
 */
typedef struct {
  // Buffer for all possible peaks of the MMR.  Only some of these will ever
  // be valid at a time.  See the bitmask below.
  //
  // peaks[0] corresponds to the lowest bit of num
  merklehash peaks[MAX_PROOF_LEN];

  // This is the number of elements in the MMR but also a bitmask of where the
  // peaks are.  Think about it for a sec and it makes sense.
  size_t num;
} merklemr;

/**
 * Makes the MMR empty.
 */
void merklemr_init_empty(merklemr *);

/**
 * Increments the number of elements in the MMR, adding the hash to it and
 * rearranging the tree.
 *
 * NB: This isn't a constant-time operation.
 */
void merklemr_add_hash(merklemr *, merklehash *);

/**
 * This is your actual merkle inclusion proof!
 */
typedef struct {
  // I say "cohashes" here because they're the root for the sibling subtree in
  // the merkle tree.
  //
  // These hashes are computed bottom-up.
  merklehash cohashes[MAX_PROOF_LEN];

  // If this is 0, then the root is just the hash of the data itself and
  // chunk_index should also be 0.
  size_t prooflen;

  // This lets you figure out how to order the current subtree hash and the
  // cohash in the node hash function.  Use the bits, Luke!
  // size_t chunk_index;
} merkleproof;

void merkle_proof_init_empty(merkleproof *);

/**
 * Adds a hash to the MMR, produces a proof (if non-NULL), and ALSO updates a
 * set of proofs for other blocks along with it.  Items in the to-update proofs
 * can be NULL, these items will just be skipped.  The vector *can* be NULL as
 * long as the length specified is 0.
 *
 * Behavior undefined if passed merkle proofs that don't belond to this MMR, or
 * if the proofs provided aren't in-sync with this MMR.  Basically, if you get a
 * proof from the MMR, every call to this function you have to provide it as a
 * proof to update, and after you stop calling it on calls to this function then
 * things are just gonna break if you try to update it again.
 *
 * NB: This isn't a constant-time operation.
 */
void merklemr_add_hash_update(merklemr *, merklehash *, merkleproof *,
                              merkleproof *, size_t *, size_t);

/**
 * If there is a single topmost root, fill that in.  If this returns nonzero
 * then the MMR doens't have a power-of-2 number of hashes OR the MMR is empty.
 *
 * This is useful for verifying merkle proofs from an MMR without using the
 * merklemr_proof_verify function, and instead using merkle_proof_verify.
 */
size_t merklemr_get_single_root(merklemr *, merklehash *);

/**
 * This compute a merkle root using a MMR construction.  Clears the merklemr and
 * uses it as a workspace.  Should have same semantics to the non-MMR version.
 */
void merkle_compute_root_mmr(merklemr *, BYTE *, size_t, merklehash *);

/**
 * Verifies a merkle proof for some hash against a root.  Returns 0 on success,
 * nonzero otherwise.
 *
 * NB: This runs in the same time for two valid proofs of the same length.
 */
size_t merkle_proof_verify(merklehash *, size_t, merklehash *, merkleproof *);

/**
 * Verifies a merkle proof for some hash against the MMR.  The MMR doesn't need
 * to have a power-of-2 number of elements in it for it to work, but the proof
 * needs to be fully updated or it will fail like any other proof.
 *
 * Returns 0 on success, nonzero otherwise.
 */
size_t merklemr_proof_verify(merklemr *, size_t, merklehash *, merkleproof *);

#endif
